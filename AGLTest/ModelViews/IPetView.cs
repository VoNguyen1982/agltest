﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AGLTest.Enums;

namespace AGLTest.ModelViews
{
    public interface IPetView<out TViewModel, in TEntity>
    {
        TViewModel ProcessView(IEnumerable<TEntity> entity);
    }
}