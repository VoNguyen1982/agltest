﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AGLTest.Data;
using AGLTest.Enums;
using AGLTest.Models;

namespace AGLTest.ModelViews
{
    public class CatModel : IPetView<IEnumerable<IGrouping<GenderType, string>>,Person>
    {
        public GenderType Gender { get; set; }
        public List<string> PetNameList { get; set; }

        private ILogService logService;

        public CatModel(ILogService _logService)
        {
            logService = _logService;
        }

        public IEnumerable<IGrouping<GenderType,string>> ProcessView(IEnumerable<Person> personList)
        {
            try
            {
                var query = personList.Where(p => p.Pets != null)
                    .SelectMany(p => p.Pets.Where(p1 => p1.Type == PetType.Cat),
                        (p, p1) => new {Gender = p.Gender, PetName = p1.Name})
                    .GroupBy(a => a.Gender, a => a.PetName);
                return query;
            }
            catch (Exception ex)
            {
                logService.Log(ex.Message);
            }
            return null;
        }
    }
}