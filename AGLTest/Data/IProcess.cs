﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;

namespace AGLTest.Data
{
    public interface IProcess<TEntity>
    {
        IEnumerable<TEntity> ProcessData(string url);
    }
}