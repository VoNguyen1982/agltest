﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;

namespace AGLTest.Data
{
    public interface ILogService
    {
        void Log(string message);
    }
}