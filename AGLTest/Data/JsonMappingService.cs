﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AGLTest.Models;
using Newtonsoft.Json;

namespace AGLTest.Data
{
    public class JsonMappingService : IMappingService<Person>
    {
        private ILogService logService;


        public JsonMappingService(ILogService _logService)
        {
            logService = _logService;
        }

        public IEnumerable<Person> Mapping(string jsonData)
        {
            try
            {
                return JsonConvert.DeserializeObject<IEnumerable<Person>>(jsonData);
            }
            catch (Exception ex)
            {
                logService.Log(ex.Message);
            }
            return null;
        }
    }
}