﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AGLTest.Data
{
    public interface IMappingService<out TEntity>
    {
        IEnumerable<TEntity> Mapping(string jsonData);
    }
}