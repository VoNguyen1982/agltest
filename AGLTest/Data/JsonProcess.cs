﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AGLTest.Models;

namespace AGLTest.Data
{
    public class JsonProcess:IProcess<Person>
    {

        private ILogService log;
        private IDataService data;
        private IMappingService<Person> mapping;

        public JsonProcess(ILogService _log, IDataService _data, IMappingService<Person> _mapping)
        {
            log = _log;
            data=new JsonDataService(log);
            mapping = new JsonMappingService(log);
        }

        public IEnumerable<Person> ProcessData(string url)
        {
            try
            {
                return mapping.Mapping(data.RetrieveData(url));
            }
            catch (Exception ex)
            {
                log.Log(ex.Message);
            }
            return null;
        }
    }
}