﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AGLTest.Models;

namespace AGLTest.Data
{
    public interface IDataService
    {
        string RetrieveData(string url);
    }
}