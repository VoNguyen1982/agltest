﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace AGLTest.Data
{
    public class JsonDataService:IDataService
    {
        private ILogService logService;

        public JsonDataService(ILogService _logService)
        {
            logService = _logService;
        }

        public string RetrieveData(string url)
        {
            using (var w = new WebClient())
            {
                var jsonData = string.Empty;
                try
                {
                    jsonData = w.DownloadString(url);
                }
                catch (Exception ex)
                {
                    logService.Log(ex.Message);
                }
                return jsonData;    
            }
        }
    }
}