﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AGLTest.Data
{
    public class ErrorLogService:ILogService
    {

        public ErrorLogService()
        {
        }

        public void Log(string message)
        {
            throw new Exception(message);
        }
    }
}