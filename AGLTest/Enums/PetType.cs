﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Antlr.Runtime.Tree;

namespace AGLTest.Enums
{
    public enum PetType
    {
        Cat, Dog, Fish
    }
}