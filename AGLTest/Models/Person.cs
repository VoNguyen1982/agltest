﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AGLTest.Enums;

namespace AGLTest.Models
{
    public class Person
    {
        public string Name { get; set; }
        public GenderType Gender { get; set; }
        public int Age { get; set; }
        public List<Pet> Pets { get; set; }
    }
}