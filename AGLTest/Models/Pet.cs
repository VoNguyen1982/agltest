﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AGLTest.Enums;

namespace AGLTest.Models
{
    public class Pet
    {
        public string Name { get; set; }
        public PetType Type { get; set; }
    }
}