﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AGLTest.Data;
using AGLTest.Enums;
using AGLTest.Models;
using AGLTest.ModelViews;

namespace AGLTest.Controllers
{
    public class HomeController : Controller
    {
        private ILogService logContext;
        private IDataService dataContext;
        private IMappingService<Person> mappingContext;
        private IPetView<IEnumerable<IGrouping<GenderType, string>>,Person> viewContext;
        private IProcess<Person> processContext;

        private string JSON_URL = ConfigurationManager.AppSettings["Test_Url"];

        public HomeController()
        {
            logContext=new ErrorLogService();
            dataContext = new JsonDataService(logContext);
            mappingContext = new JsonMappingService(logContext);
            viewContext= new CatModel(logContext);
            processContext=new JsonProcess(logContext,dataContext,mappingContext);
        }

        public ActionResult List()
        {
            return View(viewContext.ProcessView(processContext.ProcessData(JSON_URL)));
        }
    }
}
