﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AGLTest.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AGLUnitTest
{
    [TestClass]
    public class LogTest
    {
        private ILogService logContext;

        public LogTest()
        {
            logContext=new ErrorLogService();
        }

        [TestMethod]
        [ExpectedException(typeof(Exception),"Test")]
        public void ThrowException()
        {
            logContext.Log("Test");
        }
    }
}
