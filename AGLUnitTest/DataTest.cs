﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AGLTest.Data;
using AGLTest.Enums;
using AGLTest.Models;
using AGLTest.ModelViews;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AGLUnitTest
{
    [TestClass]
    public class DataTest
    {
        private ILogService logContext;
        private IDataService dataService;
        private string JSON_URL = "http://agl-developer-test.azurewebsites.net/people.json";

        public DataTest()
        {
            logContext=new ErrorLogService();
            dataService= new JsonDataService(logContext);
        }

        [TestMethod]
        public void ReturnLengthOfJson()
        {
            var result = dataService.RetrieveData(JSON_URL);
            Assert.AreEqual(616,result.Length);
        }
    }
}
