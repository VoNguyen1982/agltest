﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AGLTest.Data;
using AGLTest.Enums;
using AGLTest.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AGLUnitTest
{
    [TestClass]
    public class MappingTest
    {
        private ILogService logService;
        private IDataService dataService;
        private IMappingService<Person> mappingService;
        private List<Person> TestData;

        private string JSON_URL = "http://agl-developer-test.azurewebsites.net/people.json";

        public MappingTest()
        {
            logService = new ErrorLogService();
            dataService = new JsonDataService(logService);
            mappingService = new JsonMappingService(logService);
        }

        [TestMethod]
        public void ReturnCorrectNoRecord()
        {
            var result = mappingService.Mapping(dataService.RetrieveData(JSON_URL));
            Assert.AreEqual(6,result.Count());
        }
    }
}
