﻿using System;
using System.Collections.Generic;
using System.Linq;
using AGLTest.Data;
using AGLTest.Enums;
using AGLTest.Models;
using AGLTest.ModelViews;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AGLUnitTest
{
    [TestClass]
    public class ViewTest
    {
        
        private IPetView<IEnumerable<IGrouping<GenderType, string>>, Person> viewContext;
        private ILogService logContext;
        private List<Person> TestData;

        public ViewTest()
        {
            logContext=new ErrorLogService();
            viewContext = new CatModel(logContext);
            TestData = new List<Person>();
            TestData.Add(new Person() { Gender = GenderType.Female, Pets = new List<Pet>() { new Pet() { Type = PetType.Cat, Name = "Cat01" }, new Pet() { Type = PetType.Cat, Name = "Cat02" }, new Pet() { Type = PetType.Dog, Name = "Dog01" } } });
            TestData.Add(new Person() { Gender = GenderType.Male, Pets = new List<Pet>() { new Pet() { Type = PetType.Cat, Name = "Cat03" }, new Pet() { Type = PetType.Cat, Name = "Cat04" }, new Pet() { Type = PetType.Dog, Name = "Dog02" } } });
            TestData.Add(new Person() { Gender = GenderType.Female, Pets = new List<Pet>() { new Pet() { Type = PetType.Cat, Name = "Cat04" }, new Pet() { Type = PetType.Cat, Name = "Cat05" }, new Pet() { Type = PetType.Dog, Name = "Dog03" } } });
            TestData.Add(new Person() { Gender = GenderType.Male, Pets = new List<Pet>() { new Pet() { Type = PetType.Cat, Name = "Cat06" }, new Pet() { Type = PetType.Fish, Name = "Fish01" }, new Pet() { Type = PetType.Dog, Name = "Dog04" } } });
        }

        [TestMethod]
        public void ReturnCorrectListGenderAndCatName()
        {
            var result = viewContext.ProcessView(TestData);
            Assert.AreEqual(2,result.Count());
            Assert.AreEqual(4,result.First(a=>a.Key==GenderType.Female).Count());
            Assert.AreEqual(3, result.First(a => a.Key == GenderType.Male).Count());
            Assert.AreEqual(3, result.First(a => a.Key == GenderType.Male).Count());
        }

    }
}
