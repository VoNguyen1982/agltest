﻿using System;
using System.Collections.Generic;
using System.Linq;
using AGLTest.Data;
using AGLTest.Enums;
using AGLTest.Models;
using AGLTest.ModelViews;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AGLUnitTest
{
    [TestClass]
    public class ProcessTest
    {
        private ILogService logContext;
        private IDataService dataContext;
        private IMappingService<Person> mappingContext;
        private IPetView<IEnumerable<IGrouping<GenderType, string>>, Person> viewContext;
        private IProcess<Person> processContext;

        private string JSON_URL = "http://agl-developer-test.azurewebsites.net/people.json";

        public ProcessTest()
        {
            logContext=new ErrorLogService();
            dataContext = new JsonDataService(logContext);
            mappingContext = new JsonMappingService(logContext);
            viewContext= new CatModel(logContext);
            processContext=new JsonProcess(logContext,dataContext,mappingContext);

        }

        [TestMethod]
        public void RetreiveData_ReturnCorrectNoOfRecord()
        {
           
            var result = processContext.ProcessData(JSON_URL);
            Assert.AreEqual(6,result.Count());
        }
    }
}
